/**
 * Renvoie la somme des valeurs du tableau
 */
Array.prototype.mySomme = function() {
    let x = 0;

    for(i = 0; i < this.length; i++) {
        x += this[i];
    }
    return x;
}

/**
 * Renvoie la valeur max du tableau
 */
Array.prototype.myMax = function() {
    let x = this[0];

    for(i = 0; i < this.length; i++) {
        if(this[i] > x){
            x = this[i];
        }
    }
    return x;
}

/**
 * Renvoie le tableau transformé par la fonction passée en paramètre
 * @param {function} f
 */
Array.prototype.myTransformation = function(f) {
    x = new Array();

    for(i = 0; i < this.length; i++) {
        x[i] = f(this[i]);
    }
    return x;
}

/**
 * Renvoie le tableau avec les valeurs doublées
 */
Array.prototype.myDouble = function() {
    x = new Array();

    for(i = 0; i < this.length; i++) {
        x[i] = this[i]*2;
    }
    return x;
}

/**
 * Renvoie le tableau avec les valeurs au-dessus de la valeur seuil
 */
Array.prototype.mySeuil = function(b) {
    x = new Array();
    var j = 0;

    for(i = 0; i < this.length; i++) {
        if(this[i] > b) {
            x[j++] = this[i];
        }
    }
    return x;
}

let a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
console.log("Méthode de somme du tableau : ", a.mySomme());
console.log("Méthode renvoyant la valeur maximale : ", a.myMax());
console.log("Méthode renvoyant les valeurs doublées : ", a.myDouble());
console.log("Méthode renvoyant les valeurs au dessus du seuil fixé : ", a.mySeuil(10));
console.log("Méthode renvoyant une transformation du tableau : ", a.myTransformation(function(x){return 50*x;}));
