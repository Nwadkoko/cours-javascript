const seuil = 10;

let a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
let b = j = 0;
let c = null;
let d = new Array();
let e = new Array();

for (i = 0; i < a.length; i++) {
    b += a[i];

    if(a[i] > c) {
        c = a[i];
    }

    d[i] = a[i] * 2;
    
    if(a[i] > seuil) { 
        e[j] = a[i];
        j++;
    }
}

console.log("Somme des valeurs du tableau : ", b);
console.log("Valeur la plus grande : ", c);
console.log("Tableau avec valeurs doubles : ", d);
console.log("Tableau avec valeurs au dessus de ", seuil, " : ", e);