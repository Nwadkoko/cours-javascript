function somme(a) {
    let x = 0;

    for(i = 0; i < a.length; i++) {
        x += a[i];
    }
    return x;
}

function max(a) {
    let x = a[0];

    for(i = 0; i < a.length; i++) {
        if(a[i] > x){
            x = a[i];
        }
    }
    return x;
}

function double(a) {
    x = new Array();

    for(i = 0; i < a.length; i++) {
        x[i] = a[i]*2;
    }
    return x;
}

function seuil(a, b) {
    x = new Array();
    var j = 0;

    for(i = 0; i < a.length; i++) {
        if(a[i] > b) {
            x[j++] = a[i];
        }
    }
    return x;
}

let a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
console.log("Fonction de somme du tableau : ", somme(a));
console.log("Fonction renvoyant la valeur maximale : ", max(a));
console.log("Fonction renvoyant les valeurs doublées : ", double(a));
console.log("Fonction renvoyant les valeurs au dessus du seuil fixé : ", seuil(a, 10));
